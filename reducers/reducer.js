import { combineReducers } from 'redux';
const INITIAL_STATE={
    initialRange:['2018-04-01', '2018-04-10'],
};
const friendReducer = (state = INITIAL_STATE, action) => {
    switch (action.type) {
      default:
        return state
    }
  };
  export default combineReducers({
    friends: friendReducer,
  });