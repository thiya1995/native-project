import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View, Image,Button,placeholder,Alert} from 'react-native';
import { Provider } from 'react-redux';
import { PersistGate } from 'redux-persist/lib/integration/react';
import Display from './truckrequest.js';
import DateRangePicker from './DateRangePicker';
import Transport from './second';
import {
  createStackNavigator,
} from 'react-navigation'; 

const NavigationApp = createStackNavigator({
  Home: { screen: Display },
  Second: { screen: Transport },
});
export default class App extends Component {
  
  render() {
    
    return <NavigationApp />
  }
}


