import React, { Component } from "react";
import httpClient from "./utils/httpClient";
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Image,
  Button,
  placeholder,
  Alert
} from "react-native";
import { Header, Card, Avatar } from "react-native-elements";
import Icon from "react-native-vector-icons/FontAwesome";
import DatePicker from "react-native-date-ranges";
import moment from "moment";
import DateRangePicker from "./DateRangePicker";
import { createStackNavigator } from "react-navigation";
var now = moment().format("DD MMM YYYY");
<script src="http://localhost:8097" />;
export default class extends Component {
  static navigationOptions = {
    title: "Home"
  };
  defaultDate = [
    moment(new Date()).format("DD MMM YYYY"),
    moment(new Date()).format("DD MMM YYYY")
  ];

  constructor(props) {
    super(props);
    this.state = {
      indentCount: 0,
      indentStatus: "",
      showCal: false,
      startDate: " 2018-04-01",
      endDate: "2018-04-10",
      initialRange: ["2018-04-01", "2018-04-10"]
    };

    this.handleEvent = this.handleEvent.bind(this);
  }
  componentDidMount() {
   
    // return fetch( "https://api-preprod.blackbucklabs.com/ims/transporter/dashboard/indents/v1/filter?startDate=" +
    // this.startDate +
    // "&endDate=" +
    // this.endDate)
    //   .then(response => response.json())
    //   .then(responseJson => {
    //     console.log(responseJson);
    //   })
    //   .catch(error => {
    //     console.error(error);
    //   });
    // httpClient.getInstance()
    // .get("https://api-preprod.blackbucklabs.com/ims/transporter/dashboard/indents/v1/filter?startDate=" +
    // this.startDate +
    // "&endDate=" +
    // this.endDate)
    // .then((res)=>{
    //   console.log("jj")

    //   console.log(res)
    // })
    
    httpClient
      .getInstance()
      .get(
        "https://api-preprod.blackbucklabs.com/ims/transporter/dashboard/indents/v1/filter?startDate=" +
          this.state.startDate +
          "&endDate=" +
          this.state.endDate
      )
      .then(res => {
        console.log(res);
      });
  }

  handleEvent() {
    this.setState({
      showCal: !this.state.showCal
    });
  }
  changeDate(s, e) {
    this.setState({ startDate: s, endDate: e });
    console.log(s, e);
  }
  render() {
    const { navigate } = this.props.navigation;
    return (
      <View style={styles.container}>
        <Header
          innerContainerStyles={{
            flex: 1,
            flexDirection: "row",
            alignItems: "center"
          }}
          backgroundColor="#fff"
          rightComponent={
            <Avatar
              style={{ marginRight: 30 }}
              small
              rounded
              activeOpacity={0.7}
              size={24}
            />
          }
          leftComponent={
            <View style={{ alignItems: "center" }}>
              <Text style={styles.red}>Truck Request</Text>
            </View>
          }
          centerComponent={
            <View style={{ alignItems: "center", marginLeft: 50 }}>
              <Image source={require("./bell.png")} />
            </View>
          }
        />

        <Card>
          <View
            style={{
              flexDirection: "row",
              justifyContent: "space-between",
              alignItems: "center"
            }}
          >
            <Text date={{ flex: 1 }}>
              {this.state.startDate} - {this.state.endDate}
            </Text>

            <Icon
              cal={{ flex: 1 }}
              name={"calendar"}
              color={"grey"}
              size={20}
              onPress={() => this.handleEvent()}
            />
          </View>
        </Card>
        {this.state.showCal ? (
          <View>
            <DateRangePicker
              initialRange={["2018-04-01", "2018-04-10"]}
              onSuccess={(s, e) => this.changeDate(s, e)}
              theme={{ markColor: "red", markTextColor: "white" }}
            />
          </View>
        ) : null}
        <Card>
          <View
            style={{
              flexDirection: "row",
              justifyContent: "space-between",
              alignItems: "center"
            }}
          >
            <Image source={require("./All.png")} />
            <View style={{ marginRight: 120 }}>
              <Text style={{ fontSize: 24 }}> 615 </Text>
              <Text
                style={{ marginLeft: 10 }}
                onPress={() => navigate("Second")}
              >
                All
              </Text>
            </View>
            <Image source={require("./angle-line.png")} />
          </View>
        </Card>
        <Card>
          <View
            style={{
              flexDirection: "row",
              justifyContent: "space-between",
              alignItems: "center"
            }}
          >
            <Image source={require("./Pending.png")} />
            <View style={{ marginRight: 60 }}>
              <Text style={{ fontSize: 24 }}>615</Text>
              <Text>Pending</Text>
            </View>
            <Text>68%</Text>
            <Image source={require("./angle-line.png")} />
          </View>
        </Card>
        <Card style={{ flex: 1, flexDirection: "row" }}>
          <View
            style={{
              flexDirection: "row",
              justifyContent: "space-between",
              alignItems: "center"
            }}
          >
            <Image source={require("./Accepted.png")} />

            <View style={{ marginRight: 60 }}>
              <Text style={{ fontSize: 24 }}>286</Text>
              <Text>Accepted</Text>
            </View>

            <Text>38%</Text>
            <Image source={require("./angle-line.png")} />
          </View>
        </Card>
        <Card style={{ flex: 1, flexDirection: "row" }} onPress={{}}>
          <View
            style={{
              flexDirection: "row",
              justifyContent: "space-between",
              alignItems: "center"
            }}
          >
            <Image source={require("./Reported.png")} />
            <View style={{ marginRight: 60 }}>
              <Text style={{ fontSize: 24 }}>132</Text>
              <Text>Reported</Text>
            </View>
            <Text>24%</Text>
            <Image source={require("./angle-line.png")} />
          </View>
        </Card>
        <Card style={{ flex: 1, flexDirection: "row" }}>
          <View
            style={{
              flexDirection: "row",
              justifyContent: "space-between",
              alignItems: "center"
            }}
          >
            <Image source={require("./Cancelled.png")} />
            <View style={{ marginRight: 60 }}>
              <Text style={{ fontSize: 24 }}>10</Text>
              <Text> Cancelled</Text>
            </View>
            <Text>1%</Text>
            <Image source={require("./angle-line.png")} />
          </View>
        </Card>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#F5FCFF"
  },
  red: {
    justifyContent: "center",
    color: "black",
    fontSize: 19,
    marginLeft: 10
  }
});
