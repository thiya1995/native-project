/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View} from 'react-native';
import { Header,Card,Avatar,SearchBar } from 'react-native-elements';
import Icon from "react-native-vector-icons/FontAwesome";
import axios  from "react-native-axios";
// import { StackNavigator } from 'react-navigation';
const instructions = Platform.select({
  ios: 'Press Cmd+R to reload,\n' + 'Cmd+D or shake for dev menu',
  android:
    'Double tap R on your keyboard to reload,\n' +
    'Shake or press menu button for dev menu',
});


export default class Transport extends Component {
  static navigationOptions = {
    title: 'Transporter'
  };
  render() {
  //    const { navigate } = this.props.navigation;
    return (
      <View style={styles.container}>
         <Header
          innerContainerStyles={{flex: 1, flexDirection: 'row', alignItems:'center'}}
          backgroundColor='#fff'
          rightComponent={<View style={{alignItems
            :'center',flexDirection: 'row'}} ><Icon style={{marginRight:30,fontSize:20}} name={'bell'} color={'grey'} width={'30px'}/>
             <Avatar   size={24}
                            small
                            rounded
                            activeOpacity={0.7}
                          />
          </View>}
         
          leftComponent={<View style={{alignItems
          :'center',flexDirection: 'row'}}><Icon style={{marginRight:20,fontSize:30}} name={'angle-left'} color={'grey'} width={'30px'}/>
          <Text style={{fontSize:24}} >All</Text>
                             
          </View>}
        />
      <SearchBar style={[styles.search]}
  showLoading
  platform="android"
  placeholder='Search' />
       <Card flexDirection='row'>
         <View style={{alignItems
          :'center',flexDirection: 'row'}}>
      <View   >
           <Text >
          Indent ID: 102676
         	</Text>
       <Text >
           16MT 
         	</Text>
           <Text style={{marginTop:30}} >Bhiwandi - Kanpur</Text>
           </View>  
           <View>
           <Text  style={{marginLeft:50}}>Indent Date</Text>
           <Text  style={{marginLeft:50}}>22/8/2018</Text>
           <Text style={{marginLeft:50,marginTop:15}} >Expiry Date</Text>
           <Text  style={{marginLeft:50}}>22/8/2018</Text>
           </View>
        
  </View>
</Card>
<Card flexDirection='row'>
         <View style={{alignItems
          :'center',flexDirection: 'row'}}>
      <View   >
           <Text >
          Indent ID: 102676
         	</Text>
       <Text >
           16MT 
         	</Text>
           <Text style={{marginTop:30}} >Bhiwandi - Kanpur</Text>
           </View>  
           <View>
           <Text  style={{marginLeft:50}}>Indent Date</Text>
           <Text  style={{marginLeft:50}}>22/8/2018</Text>
           <Text style={{marginLeft:50,marginTop:15}} >Expiry Date</Text>
           <Text  style={{marginLeft:50}}>22/8/2018</Text>
           </View>
        
  </View>
</Card>
<Card flexDirection='row'>
         <View style={{alignItems
          :'center',flexDirection: 'row'}}>
      <View   >
           <Text >
          Indent ID: 102676
         	</Text>
       <Text >
           16MT 
         	</Text>
           <Text style={{marginTop:30}} >Bhiwandi - Kanpur</Text>
           </View>  
           <View>
           <Text  style={{marginLeft:50}}>Indent Date</Text>
           <Text  style={{marginLeft:50}}>22/8/2018</Text>
           <Text style={{marginLeft:50,marginTop:15}} >Expiry Date</Text>
           <Text  style={{marginLeft:50}}>22/8/2018</Text>
           </View>
        
  </View>
</Card>
<Card flexDirection='row'>
         <View style={{alignItems
          :'center',flexDirection: 'row'}}>
      <View   >
           <Text >
          Indent ID: 102676
         	</Text>
       <Text >
           16MT 
         	</Text>
           <Text style={{marginTop:30}} >Bhiwandi - Kanpur</Text>
           </View>  
           <View>
           <Text  style={{marginLeft:50}}>Indent Date</Text>
           <Text  style={{marginLeft:50}}>22/8/2018</Text>
           <Text style={{marginLeft:50,marginTop:15}} >Expiry Date</Text>
           <Text  style={{marginLeft:50}}>22/8/2018</Text>
           </View>
        
  </View>
</Card>
<Card flexDirection='row'>
         <View style={{alignItems
          :'center',flexDirection: 'row'}}>
      <View   >
           <Text >
          Indent ID: 102676
         	</Text>
       <Text >
           16MT 
         	</Text>
           <Text style={{marginTop:30}} >Bhiwandi - Kanpur</Text>
           </View>  
           <View>
           <Text  style={{marginLeft:50}}>Indent Date</Text>
           <Text  style={{marginLeft:50}}>22/8/2018</Text>
           <Text style={{marginLeft:50,marginTop:15}} >Expiry Date</Text>
           <Text  style={{marginLeft:50}}>22/8/2018</Text>
           </View>
        
  </View>
</Card>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#F5FCFF',
  },
 search:{
   backgroundColor: 'white'
 }
});
